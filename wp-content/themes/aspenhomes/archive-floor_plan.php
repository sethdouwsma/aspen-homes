<?php 
// Floor Plan Archive Template
?>

<?php get_header(); ?>

	<header class="lwood">
		<div class="container">
		<?php if( get_field('hero_header', 7) ): ?>
			<h1><?php the_field('hero_header', 7); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php the_field('hero_paragraph', 7); ?>
		</div>
	</header><!-- end header -->
	
	<div id="main">

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content section container">
				<h3>Search Results:</h3>
				<ul class="floorplan_buckets">
					<?php
						$args = array( 'post_type' => 'floor_plan' );
						$args = array_merge( $args, $wp_query->query );
						query_posts( $args );
					?>
				  	<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'floorplan' ); ?>
						
						<?php endwhile; ?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
		
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</ul><!-- end floorplans -->
			
				<?php get_sidebar('floor_plan'); ?>
			</div><!-- end content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>