<?php
/**
 * The default template for displaying single community content
 *
 */
?>

<?php 
	$thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
	$thumb_url = $thumb_url_array[0];
?>

<div id="<?php echo $post->post_name?>" class="single-community">
	<div class="row">
		<div class="sing_comm_desc grid-6 dwood">
			<h3><?php the_title(); ?></h3>
			<hr>
			<?php the_field('description'); ?>
			<ul class="sing_comm_buttons clearfix">
				<li><a href="<?php echo get_page_link(7); ?>/?bedrooms=&bathrooms=&sqft=&community=<?php echo $post->post_name; ?>&aspen-search=1" class="button-gold-stroked">Available Floor Plans for Build</a></li>
				<li><a href="#" data-community="<?php the_title(); ?>" class="popup_button button-gold-stroked">Ask About This Community</a></li>
				<?php if(get_field('information_packet')): ?>
					<li><a href="<?php the_field('information_packet'); ?>" class="information_packet" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/booklet.svg"> Information Packet</a></li>
				<?php endif; ?>
			</ul>
		</div>
		<div class="sing_comm_featured_image grid-6" style="background-image: url(<?php echo $thumb_url;?>);"></div>
	</div>
	<div class="row">
		<div class="sing_comm_map grid-6">
			<div class="map_canvas" style="background-image:url(<?php echo get_template_directory_uri();?>/images/map_it_bg.jpg)">
			
				<?php $map = get_field('google_map', get_the_ID()); ?>
				
				<a href="http://maps.google.com/maps?z=11&q=loc:<?php echo $map['address']; ?>" target="_blank">
					<div class="container">
						<img src="<?php echo get_template_directory_uri(); ?>/images/map_marker.svg">
						<h4>Map it</h4>
						<h5><?php the_title(); ?></h5>
					</div>
				</a>
			</div>			
		</div>
		<div class="sing_comm_extras grid-6">
			<?php if( have_rows('community_highlights') ): ?>
				<ul class="sing_comm_highlights">
				<h3>Community Highlights</h3>
				<hr>
			    <?php while( have_rows('community_highlights') ): the_row(); ?>
					<li class="sing_comm_highlight"><?php the_sub_field('highlight'); ?></li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
			
			<?php if( have_rows('secondary_images') ): ?>
			<ul class="sing_comm_images clearfix">
			    <?php while( have_rows('secondary_images') ): the_row(); ?>
			    	<?php $image = get_sub_field('image'); ?>
					<?php if($image): //dont output an empty image tag ?>
					<li><a href="<?php echo $image['url']; ?>" data-lightbox="<?php echo $post->ID; ?>" data-title="<?php echo $image['caption']; ?>"><img src="<?php echo $image['sizes']['secondary-thumb']; ?>" alt="<?php echo $image['caption']; ?>" /></a></li>
					<?php endif; ?>
			    <?php endwhile; ?>
			</ul>
		<?php endif; ?>
		</div>
	</div>
	
	<?php
		global $post;
		$community = $post->post_name;
	 	$args2 = array(
	 		'taxonomy' 			=> 'community',
	 		'term'				=> $community,
	 		'post_type'      	=> 'move_in_ready',
		    'order'             => 'ASC',
		    'orderby'           => 'menu_order',
		    'posts_per_page'    => 4
		);
		$mi_query = null;
		$mi_query = new WP_Query($args2);
	?>
	
	<?php if( $mi_query->have_posts() ) : ?>
		<div class="small_section sing_comm_mi_buckets lwood">
			<div class="container">
				<h3>Move-In Ready Homes Available</h3>
				<hr>
				<ul class="clearfix">
					<?php while ($mi_query->have_posts()) : $mi_query->the_post(); ?>
						<li class="mi_bucket">
							<a href="<?php echo the_permalink(); ?>">
								<div class="mi_bucket_image"><?php the_post_thumbnail('secondary-thumb'); ?></div>
								<div class="mi_bucket_info">
									<div class="mi_meta clearfix">
										<p class="mi_floorplan"><?php the_field('floor_plan_model'); ?></p>
										<p class="mi_price"><?php if(get_field('price')) { $field = get_field('price'); echo '$'.number_format($field); } ?></p>
									</div>
									<address>
										<p><?php the_field('address'); ?></p>
										<p><?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip_code'); ?>
									</address>
								</div>
							</a>
						</li>
					<?php endwhile; ?>
				</ul>
			</div>
		</div>
    <?php endif; ?>
	
</div><!-- end single-comm -->