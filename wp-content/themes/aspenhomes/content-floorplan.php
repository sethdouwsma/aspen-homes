<?php
/**
 * The default template for displaying post content
 *
 */
?>


<?php 
	$thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
	$thumb_url = $thumb_url_array[0];
?>

<li class="fp_bucket">
	<div class="fp_bucket_image" style="background-image: url(<?php echo $thumb_url;?>);">
	</div>
	<div class="fp_bucket_info_wrapper dwood">
		<div class="fp_bucket_header">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<?php if( get_field('feature_this_floor_plan')): ?>
				<h5>Featured</h5>
			<?php endif; ?>
			<hr>
		</div>
		<ul class="fp_bucket_info">
			<p class="sqft"><em><?php $field = get_field('square_feet'); echo number_format($field); ?> sq. ft.</em></p>
			<li><?php the_field('bedrooms'); ?> Bedrooms</li>
			<li><?php the_field('bathrooms'); ?> Bathrooms</li>
			<li><?php the_field('car_garage'); ?> Car Garage</em></li>
			<?php if( have_rows('extras') ): ?>
			    <?php while( have_rows('extras') ): the_row(); ?>
			        <li><?php the_sub_field('extra'); ?></li>
			    <?php endwhile; ?>
			<?php endif; ?>
		</ul>
		<p class="fp_bucket_button"><a href="<?php echo get_permalink(); ?>" class="button-gold-stroked">View Floor Plan</a></p>
	</div>
</li>