<?php
/**
 * The default template for displaying move-in ready buckets single community content
 *
 */
?>

<li class="mi_bucket">
	<?php the_post_thumbnail('movein-bucket'); ?>
</li>