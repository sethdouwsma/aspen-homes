<?php
/**
 * The default template for displaying post content
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<aside class="entry_sidebar">
		<p class="entry_date"><?php echo get_the_date(); ?></p>
		<div class="entry_share <?php if (is_single()) { print("copper"); } ?>">
			<?php echo do_shortcode('[ssba]'); ?>
		</div>
	</aside>
	<div class="entry_content_wrapper">
		<div class="entry_header">
			<?php if ( !is_single() ) : ?>
				<h2 class="entry_title"><a href="<?php echo get_permalink();?>"><?php the_title(); ?></a></h2>
			<?php else : ?>
				<h2 class="entry_title"><?php the_title(); ?></h2>
			<?php endif; ?>
			<p class="entry_meta">Posted in <?php the_category(', ') ?></p>
		</div>
		<div class="entry_content">		
			<?php if ( is_single() ) : ?>
				<?php the_content(); ?>
			<?php else : ?>
				<?php the_excerpt(); ?>
				<p class="entry_button"><a href="<?php echo get_permalink(); ?>" class="button-gold-stroked">Read More</a></p>
			<?php endif; ?>
		</div>
		
		<?php if ( is_single() ) : ?>
			<div class="post_navigation clearfix">
				<div class="post_nav_item previous"><?php previous_post_link('%link') ?></div>
				<div class="post_nav_item next"><?php next_post_link('%link') ?></div>
			</div><!-- end post_navigation -->
		<?php endif; ?>
	</div>	
</article>