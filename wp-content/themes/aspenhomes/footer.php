<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 */
?>
	
		</div><!-- end #main -->
				
		<footer>
			<div class="container">
				<div class="upper_footer">
					<div class="footer_section">
						<h4>Aspen Homes</h4>
						<ul class="footer_nav">
							<?php wp_list_pages('title_li=&exclude=5&depth=1'); ?>
						</ul>
					</div>
					<div class="footer_section">
						<h4>Connect</h4>
						<?php if( have_rows('social_links',19) ): ?>
							<ul class="social clearfix">
						    <?php while( have_rows('social_links',19) ): the_row(); ?>
								<li><a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php the_sub_field('icon'); ?>"></a></li>
						    <?php endwhile; ?>
						    </ul>
						<?php endif; ?>
						<!-- Begin MailChimp Signup Form -->
						<div id="mc_embed_signup">
						<h4>Newsletter Signup</h4>
						<form action="//aspenhomes.us3.list-manage.com/subscribe/post?u=7105cc6cf06e9f3061a82211a&amp;id=c80a486a2c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							
						<div class="mc-field-group">
							<input type="email" placeholder="Email Address..." name="EMAIL" class="required email" id="mce-EMAIL">
						</div>
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;"><input type="text" name="b_7105cc6cf06e9f3061a82211a_c80a486a2c" tabindex="-1" value=""></div>
						    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button-gold-solid"></div>
						</form>
						</div>
						<!--End mc_embed_signup-->
					</div>
					<div class="footer_section">
						<h4>Contact</h4>
						<ul class="footer_contact">
							<li>Office: <?php the_field('office_number', 19); ?></li>
							<li>Fax: <?php the_field('fax_number', 19); ?></li>
							<li>
								<address>
									<p><?php the_field('address',19); ?></p>
									<p><?php the_field('city',19); ?>, <?php the_field('state',19); ?> <?php the_field('zip_code',19); ?></p>
								</address>
							</li>
						</ul>
					</div>
					<div class="footer_badge">
						<img src="<?php echo get_template_directory_uri(); ?>/images/work_with_the_best.png" alt="Work With The Best. Best of 2014.">
					</div>
				</div>
				
				<div class="lower_footer">
					<div class="copyright">
						<p>&copy; <?php echo date('Y'); ?> Aspen Homes, LLC. All Rights Reserved.</p>
					</div>
					<div class="footer_logo">
						<p><img src="<?php echo get_template_directory_uri(); ?>/images/footer_logo.svg" alt="Aspen Homes"></p>
					</div>
					<div class="credentials">
						<p>Site Crafted By <a href="http://edencreative.co" alt="Eden Creative" target="_blank">Eden</a></p>
					</div>
				</div>
			</div>
		</footer>
	
	</div><!-- #page_container -->
	
	<?php wp_footer(); ?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/lightbox.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.singlePageNav.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.glide.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
	<!-- <script type="text/javascript" src="js/scripts.min.js"></script> -->
		
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>        
    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);
    
        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var myLatlng = new google.maps.LatLng(47.683605, -116.794046);
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 15,

                // The latitude and longitude to center the map (always required)
                center: myLatlng,
                disableDefaultUI: false,
				scrollwheel: false,

                // How you would like to style the map. 
                // This is where you would paste any style found on Snazzy Maps.
                styles: [
					    {
					        "featureType": "administrative",
					        "stylers": [
					            {
					                "visibility": "on"
					            }
					        ]
					    },
					    {
					        "featureType": "poi",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "water",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "transit",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "landscape",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "stylers": [
					            {
					                "visibility": "on"
					            }
					        ]
					    },
					    {
					        "featureType": "road.local",
					        "stylers": [
					            {
					                "visibility": "on"
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "visibility": "on"
					            }
					        ]
					    },
					    {
					        "featureType": "water",
					        "stylers": [
					            {
					                "color": "#84afa3"
					            },
					            {
					                "lightness": 52
					            }
					        ]
					    },
					    {
					        "stylers": [
					            {
					                "saturation": -77
					            }
					        ]
					    },
					    {
					        "featureType": "road"
					    }
					]
            };

            // Get the HTML DOM element that will contain your map 
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using out element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            
            var image = '<?php echo get_template_directory_uri(); ?>/images/map_marker.png';
			var marker = new google.maps.Marker({
				position: myLatlng,
				icon: image,
				map: map,
				title: 'Aspen Homes'
			});
        }
    </script>
		
</body>
</html>
