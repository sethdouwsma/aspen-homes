<?php
/**
 * joshlavender functions and definitions
 *
 * @package joshlavender
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/*
 * Load Jetpack compatibility file.
 */
require( get_template_directory() . '/inc/jetpack.php' );

if ( ! function_exists( 'joshlavender_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function joshlavender_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );

	/**
	 * Customizer additions
	 */
	require( get_template_directory() . '/inc/customizer.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on joshlavender, use a find and replace
	 * to change 'joshlavender' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'joshlavender', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'joshlavender' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
}
endif; // joshlavender_setup
add_action( 'after_setup_theme', 'joshlavender_setup' );

/**
 * Setup the WordPress core custom background feature.
 *
 * Use add_theme_support to register support for WordPress 3.4+
 * as well as provide backward compatibility for WordPress 3.3
 * using feature detection of wp_get_theme() which was introduced
 * in WordPress 3.4.
 *
 * @todo Remove the 3.3 support when WordPress 3.6 is released.
 *
 * Hooks into the after_setup_theme action.
 */
function joshlavender_register_custom_background() {
	$args = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);

	$args = apply_filters( 'joshlavender_custom_background_args', $args );

	if ( function_exists( 'wp_get_theme' ) ) {
		add_theme_support( 'custom-background', $args );
	} else {
		define( 'BACKGROUND_COLOR', $args['default-color'] );
		if ( ! empty( $args['default-image'] ) )
			define( 'BACKGROUND_IMAGE', $args['default-image'] );
		add_custom_background();
	}
}
add_action( 'after_setup_theme', 'joshlavender_register_custom_background' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function joshlavender_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'joshlavender' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'joshlavender_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function joshlavender_scripts() {
	
}
add_action( 'wp_enqueue_scripts', 'joshlavender_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );

function show_post($path) {
  $post = get_page_by_path($path);
  $content = apply_filters('the_content', $post->post_content);
  echo $content;
}

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js", false, null);
   wp_enqueue_script('jquery');
}


/* SVG SUPPORT */
function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );


/* Images */
if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );

	if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'secondary-thumb', 240, 178, true );
	add_image_size( 'floorplan-bucket', 300, 175, true );
	add_image_size( 'floorplan-large', 850, 500, true );
	add_image_size( 'movein-bucket', 1200, 800, true );
	add_image_size( 'movein-medium', 500, 300, true );
	add_image_size( 'banner-logo', 108, 70, true );
}


/* Custom Post Types */
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'floor_plan',
		array(
			'labels' => array(
				'name' => __( 'Floor Plans' ),
				'singular_name' => __( 'Floor Plan' )
			),
		'public' => true,
		'has_archive' => false,
		'taxonomies' => array('community'),
		'rewrite' => array('slug' => 'floor-plans', 'with_front' => false),
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		)
	);
	register_post_type( 'move_in_ready',
		array(
			'labels' => array(
				'name' => __( 'Move-In Ready' ),
				'singular_name' => __( 'Move-In Ready' )
			),
		'public' => true,
		'has_archive' => false,
		'taxonomies' => array('community'),
		'rewrite' => array('slug' => 'move-in-ready', 'with_front' => false),
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		)
	);
	register_post_type( 'gallery',
		array(
			'labels' => array(
				'name' => __( 'Galleries' ),
				'singular_name' => __( 'Gallery' )
			),
		'public' => true,
		'has_archive' => false,
		'taxonomies' => array('gallery'),
		'rewrite' => array('slug' => 'gallery', 'with_front' => false),
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		)
	);
}
add_action( 'init', 'create_post_type' );

/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'bt_flush_rewrite_rules' );

/* Flush your rewrite rules */
function bt_flush_rewrite_rules() {
     flush_rewrite_rules();
}


/* Custom Taxonomies */
function add_custom_taxonomies() {
  register_taxonomy('community', 'post', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    'show_in_nav_menus' => false,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'Communities', 'taxonomy general name' ),
      'singular_name' => _x( 'Community', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Community' ),
      'all_items' => __( 'All Communities' ),
      'parent_item' => __( 'Parent Community' ),
      'parent_item_colon' => __( 'Parent Community:' ),
      'edit_item' => __( 'Edit Community' ),
      'update_item' => __( 'Update Community' ),
      'add_new_item' => __( 'Add New Community' ),
      'new_item_name' => __( 'New Community Name' ),
      'menu_name' => __( 'Communities' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'communities-taxo', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'query_var' => true, 
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
  register_taxonomy('gallery', 'post', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    'show_in_nav_menus' => false,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'Gallery Categories', 'taxonomy general name' ),
      'singular_name' => _x( 'Gallery', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Gallery' ),
      'all_items' => __( 'All Galleries' ),
      'parent_item' => __( 'Parent Gallery' ),
      'parent_item_colon' => __( 'Parent Gallery:' ),
      'edit_item' => __( 'Edit Gallery' ),
      'update_item' => __( 'Update Gallery' ),
      'add_new_item' => __( 'Add New Gallery' ),
      'new_item_name' => __( 'New Gallery Name' ),
      'menu_name' => __( 'Gallery Categories' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'galleries', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}
add_action( 'init', 'add_custom_taxonomies', 0 );


// Floor Plan Custom Columns
add_filter( 'manage_edit-floor_plan_columns', 'my_edit_floor_plan_columns' ) ;
function my_edit_floor_plan_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Floor Plan' ),
		'sqft' => __( 'Sq. Ft.' ),
		'date' => __( 'Date' )
	);
	return $columns;
}

add_action( 'manage_floor_plan_posts_custom_column', 'my_manage_floor_plan_columns', 10, 2 );
function my_manage_floor_plan_columns( $column, $post_id ) {
	global $post;
	switch( $column ) {
		case 'sqft' :
			if(get_field('square_feet'))
			{
				$sqft = get_field('square_feet');
				$sqft_format = number_format($sqft);
				echo $sqft_format;
			}
			else
			{
				echo 'N/A';
			}
			break;
			
		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

add_filter( 'manage_edit-floor_plan_sortable_columns', 'my_sortable_floor_plan_column' );
function my_sortable_floor_plan_column( $columns ) {
    $columns['sqft'] = 'sqft';
 
    //To make a column 'un-sortable' remove it from the array
    //unset($columns['date']);
 
    return $columns;
}
add_action( 'pre_get_posts', 'my_floor_plan_orderby' );
function my_floor_plan_orderby( $query ) {
    if( ! is_admin() )
        return;
 
    $orderby = $query->get( 'orderby');
 
    if( 'sqft' == $orderby ) {
        $query->set('meta_key','square_feet');
        $query->set('orderby','meta_value_num');
    }
}


// Move-In Ready Columns
add_filter( 'manage_edit-move_in_ready_columns', 'my_edit_move_in_ready_columns' ) ;
function my_edit_move_in_ready_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Address' ),
		'price' => __( 'Price' ),
		'floorplan' => __( 'Floor Plan' ),
		'community' => __( 'Community' ),
		'date' => __( 'Date' )
	);
	return $columns;
}

add_action( 'manage_move_in_ready_posts_custom_column', 'my_manage_move_in_ready_columns', 10, 2 );
function my_manage_move_in_ready_columns( $column, $post_id ) {
	global $post;
	switch( $column ) {
		case 'community' :
			/* Get the genres for the post. */
			$terms = get_the_terms( $post_id, 'community' );
			/* If terms were found. */
			if ( !empty( $terms ) ) {
				$out = array();
				/* Loop through each term, linking to the 'edit posts' page for the specific term. */
				foreach ( $terms as $term ) {
					$out[] = sprintf( '<a href="%s">%s</a>',
						esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'community' => $term->slug ), 'edit.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'community', 'display' ) )
					);
				}
				/* Join the terms, separating them with a comma. */
				echo join( ', ', $out );
			}
			/* If no terms were found, output a default message. */
			else {
				_e( 'No Communities' );
			}
			break;
			
		case 'floorplan' :
			if(get_field('floor_plan_model'))
			{
				$floorplan = get_field('floor_plan_model');
				echo $floorplan;
			}
			else
			{
				echo 'N/A';
			}
			break;
			
		case 'price' :
			if(get_field('price'))
			{
				$price = get_field('price');
				$price_format = number_format($price);
				echo '$ '.$price_format;
			}
			else
			{
				echo 'N/A';
			}
			break;
			
		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

// Galleries Custom Columns
add_filter( 'manage_edit-gallery_columns', 'my_edit_gallery_columns' ) ;
function my_edit_gallery_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Gallery' ),
		'gallery' => __( 'Category' ),
		'date' => __( 'Date' )
	);
	return $columns;
}

add_action( 'manage_gallery_posts_custom_column', 'my_manage_gallery_columns', 10, 2 );
function my_manage_gallery_columns( $column, $post_id ) {
	global $post;
	switch( $column ) {
		case 'gallery' :
			/* Get the genres for the post. */
			$terms = get_the_terms( $post_id, 'gallery' );
			/* If terms were found. */
			if ( !empty( $terms ) ) {
				$out = array();
				/* Loop through each term, linking to the 'edit posts' page for the specific term. */
				foreach ( $terms as $term ) {
					$out[] = sprintf( '<a href="%s">%s</a>',
						esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'gallery' => $term->slug ), 'edit.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'gallery', 'display' ) )
					);
				}
				/* Join the terms, separating them with a comma. */
				echo join( ', ', $out );
			}
			/* If no terms were found, output a default message. */
			else {
				_e( 'No Galleries' );
			}
			break;
			
		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

// Change Wordpress Gallery Output 
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    // Here's your actual output, you may customize it to your need
    $output = "<div class=\"slideshow-wrapper\">\n";
    $output .= "<div class=\"preloader\"></div>\n";
    $output .= "<ul class=\"images\" data-orbit>\n";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        // Fetch the thumbnail (or full image, it's up to you)
//      $img = wp_get_attachment_image_src($id, 'medium');
//      $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
        $img = wp_get_attachment_image_src($id, 'full');

        $output .= "<li class=\"item\">\n";
        $output .= "<a href=\"{$img[0]}\" data-lightbox='gallery-images' data-title=\"$attachment->post_excerpt\"><img src=\"{$img[0]}\" width=\"{$img[1]}\" height=\"{$img[2]}\" alt=\"\" /></a>\n";
        $output .= "</li>\n";
    }

    $output .= "</ul>\n";
    $output .= "</div>\n";

    return $output;
}

// Select All Communities
add_action( 'admin_print_footer_scripts', 'wpa104168_js_solution' );

function wpa104168_js_solution(){ ?>

<script type="text/javascript">

jQuery(document).ready(function($) {

    $('ul#communitychecklist').prepend('<li><label class="selectit"><input type="checkbox" class="toggle-all-terms"/> Select All</label>');

    $('.toggle-all-terms').on('change', function(){
        $(this).closest('ul').find(':checkbox').prop('checked', this.checked );
    });

});
</script>

<?php }


// Load Floor Plans into Select Box
wpcf7_add_shortcode('postdropdown', 'createbox', true);
function createbox(){
global $post;
$terms = get_terms( 'community', 'orderby=count&hide_empty=0&exclude=56,57,63,58' );
$output = "<select name='cursus' id='cursus' onchange='document.getElementById(\"cursus\").value=this.value;'><option value='Select Community Interests' disabled selected>Select Community Interest</option>";
foreach ( $terms as $term ) : setup_postdata($post);
	$title = $term->name;
	$output .= "<option value='$title'> $title </option>";

	endforeach;
$output .= "</select>";
return $output;
}