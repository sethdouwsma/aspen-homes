<!DOCTYPE HTML>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>

	<meta charset="utf-8">
	
	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php wp_title('|',true,'right'); ?></title>
	
	<!-- mobile meta (hooray!) -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no"/>
	
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="/favicon.ico">
	<![endif]-->
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css">
	
	<!--[if IE]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.css" type="text/css" media="screen"><![endif]-->
	<!--[if IE]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<!-- drop Google Analytics Here -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-19082196-40', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');
	
	</script>
	<!-- end analytics -->

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<div id="mobile_nav">
		<div class="overlay_bg"></div>
		<div class="container">
			<a href="#" class="close_popup">&times;</a>
			<ul class="navigation">
				<?php wp_list_pages('title_li=&depth=1'); ?>
				<li class="pv_nav"><a href="#" class="pj_button button-gold-solid" target="_blank">Project Viewer</a></li>
			</ul>
		</div>
	</div>
	
	<nav class="nav_container sticky clearfix">
		<div class="logo">
			<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" alt="Aspen Homes - A Custom Home Builder in Coeur d'Alene, Idaho" /></a>
		</div>
		<p class="header_toggle"><img src="<?php echo get_template_directory_uri(); ?>/images/header_toggle.svg"></p>
		<ul class="navigation">
			<?php wp_list_pages('title_li=&depth=1'); ?>
			<li class="pv_nav"><a href="#" class="pj_button button-gold-solid" target="_blank">Project Viewer</a></li>
		</ul>
	</nav>
	
	<div id="form_popup">
		<div class="overlay_bg"></div>
		<div class="container">
			<div class="login">
				<iframe id="pj" src="http://www.buildertrend.net/loginFrame.aspx?builderID=809" marginwidth="0" marginheight="0" frameborder="0"></iframe>
			</div>
		</div>
	</div>
	
	<div id="page_container">
