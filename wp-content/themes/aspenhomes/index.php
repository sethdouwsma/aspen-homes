<?php get_header(); ?>
	
	<header class="lwood">
		<div class="container">
		<?php if( get_field('hero_header',17) ): ?>
			<h1><?php the_field('hero_header',17); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php the_field('hero_paragraph',17); ?>
		</div>
	</header><!-- end header -->

	<div class="content">	
				
		<?php if ( have_posts() ) : ?>
		
			<?php while ( have_posts() ) : the_post(); ?>

				<?php 
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
					$thumb_url = $thumb_url_array[0];
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="background-image: url(<?php echo $thumb_url;?>);">
					<div class="overlay"></div>
					<div class="container">
						<aside class="entry_sidebar">
							<p class="entry_date"><?php echo get_the_date(); ?></p>
							<div class="entry_share <?php if (is_home()) { print("yellow"); } ?>">
								<?php echo do_shortcode('[ssba]'); ?>
							</div>
						</aside>
						<div class="entry_content_wrapper">
							<div class="entry_header">
								<h2 class="entry_title"><a href="<?php echo get_permalink();?>"><?php the_title(); ?></a></h2>
								<p class="entry_meta">Posted in <?php the_category(', ') ?></p>
							</div>
							<div class="entry_content">
								<?php the_excerpt(); ?>
								<p class="entry_button"><a href="<?php echo get_permalink(); ?>" class="button-gold-stroked">Read More</a></p>
							</div>
						</div>	
					</div>
				</article>
			<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
		
	</div><!-- end .content -->

<?php get_footer(); ?>