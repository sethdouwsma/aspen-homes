jQuery(document).ready(function ($) {

	var sHeight = $('.gallery_buckets_wrapper .gallery_buckets.active').height();
	$('.gallery_buckets_wrapper').css("height", sHeight);
	
	var navHeight = $('.secondary_nav_wrapper').outerHeight();
	$('.single-community .row').css("padding-top", navHeight).css("margin-top", (navHeight*-1));
	
	var scroll = $(window).scrollTop();
	var header = $('header').outerHeight();

    if(scroll  > header && $('body').hasClass('page-template-page-city-community-php')) {
	     $('.page-template-page-city-community-php .secondary_nav_wrapper').removeClass('not_sticky').addClass('sticky'); 
	     $('nav').removeClass('sticky').addClass('not_sticky'); 
	     $('.content').css("padding-top", $('.secondary_nav_wrapper').outerHeight());
    }

	// Small Nav
	$(window).scroll(function () {
		if ($(window).width() > 1024) {
			$('.nav_container').toggleClass('small', $(document).scrollTop() > 50);
		}
		
		/*
		var el = $('.fp_filters'),
			top = $('.content').offset().top - $(document).scrollTop();
	    if (top < 30 && !el.is('.fixed')){
	        $(el).addClass('fixed');
	    }
	    if (top > 30 && el.is('.fixed')){
	        $(el).removeClass('fixed');
	    } 
		*/ 
    });
    
	if ($(window).width() <= 1024) {
		$( '.nav_container' ).addClass("small");
   	}
    
    $(window).resize(function() {
    	if ($(window).width() <= 1024) {
			$( '.nav_container' ).addClass("small");
	   	} else {
			$('.nav_container').removeClass('small');
	   	}
	});
		
	// Header toggle
    $('.header_toggle').click(function(e) {
    	e.preventDefault();
		$('#mobile_nav').fadeIn(300);
	});
	
	$('.header_toggle_brown').click(function() {
		$('.secondary_nav_wrapper').toggleClass( "toggled" );
	});
	
	$('.secondary_nav_wrapper .secondary_nav a').click(function() {
		if ($(window).width() <= 767) {
			$('.secondary_nav_wrapper').toggleClass( "toggled" );
		}
	});
	
	$('.close_popup').click(function(e) {
		e.preventDefault();
		$('#mobile_nav').fadeOut(300);
	});
	
	$('.overlay_bg').click(function() {
		$('#mobile_nav').fadeOut(300);
	});
	// Video button 	
	$( '.scroll_down' ).click(function(e) {
		e.preventDefault();
		$('html, body').animate({
		    scrollTop: ($('#main').first().offset().top)
		},400);
	});
	
	// Sliders
    $('.hp_news_slider').glide({
        autoplay: false,
        arrows: '.hp_news',
        navigation: false
    });
	
    $('.testimonial_wrapper .slider').glide({
        autoplay: 4000,
        arrows: false,
        navigation: false
    });
	
	// FAQ's
	$('.faq_list').accordion({ heightStyle: "content" });
	
	// Single Page Nav
	$('.secondary_nav').singlePageNav();
	
	$(window).scroll(function(){
	     var top 	= $(this).scrollTop();
	     var height	= $('header').height();
	     	 
	     if(top >= height && $('body').hasClass('page-template-page-city-community-php')) {
	         $('.page-template-page-city-community-php .secondary_nav_wrapper').removeClass('not_sticky').addClass('sticky'); 
	         $('nav').removeClass('sticky').addClass('not_sticky'); 
	         $('.content').css("padding-top", $('.secondary_nav_wrapper').outerHeight());
	     }
	     
	     if(top < height && $('.secondary_nav_wrapper').hasClass('sticky')) {
	         $('.secondary_nav_wrapper').removeClass('sticky').addClass('not_sticky');
	         $('nav').removeClass('not_sticky').addClass('sticky');  
	         $('.content').css("padding-top", 0); 
	     }
	});
	
	// Hero heights
	$(function() {
		$('body.home header').css( 'height', $( window ).height());
		$('.video_container').css( 'height', $( window ).height());
		$('body.page-id-13 header').css( 'height', $( window ).height());
	});
	
	$( window ).resize(function() {
		$('body.home header').css( 'height', $( window ).height());
		$('.video_container').css( 'height', $( window ).height());
		$('body.page-id-13 header').css( 'height', $( window ).height());
	});
	
	// Popup 
	$('.popup_button').click(function(e) {
		e.preventDefault();
		$('#popup').addClass("opened");
	}); 
	
	$('#video_button').click(function(e) {
		e.preventDefault();
    	$('#pj_video').show().addClass("current").attr("src", "//www.youtube.com/embed/Bl_wnbqV3iI?autoplay=1");
	}); 
	
	$('.close_popup').click(function(e) {
		e.preventDefault();
		$('#popup').removeClass("opened");
    	$('#pj_video').attr("src", "");
	});
	
	$('.overlay_bg').click(function() {
		$('#popup').removeClass("opened");
    	$('#pj_video').attr("src", "");
	});
	
	$('.pj_button').click(function(e) {
		e.preventDefault();
		$('#form_popup').addClass("opened");
	}); 
	
	$('#form_popup .close_popup').click(function(e) {
		e.preventDefault();
		$('#form_popup').removeClass("opened");
	});
	
	$('#form_popup .overlay_bg').click(function() {
		$('#form_popup').removeClass("opened");
	});
	
	$('.sing_comm_desc .popup_button').click(function() {
		var title = $(this).data('community');
		$('#form_title').val(title).attr('readonly', true);
	});
	
	$('.sing_comm_desc .information_packet').click(function(e) {
		e.preventDefault();
		$('.information_packet').removeClass("link");
		$(this).addClass("link");
		$('#mc_popup').addClass("opened");
	});

	$('.floorplan_buttons .popup_button').click(function() {
		var title =  $(this).attr('href');
		$('#form_title').val(title).attr('readonly', true);
	});
	
	function popup_message() {
		$('.form_header').css("display", "none");
	}
	
	$('.mc_button').click(function(e) {
		e.preventDefault();
		$('#mc_popup').addClass("opened");
		
	}); 
	
	$('#mc_popup .close_popup').click(function(e) {
		e.preventDefault();
		$('#mc_popup').removeClass("opened");
	});
	
	$('#mc_popup .overlay_bg').click(function() {
		$('#mc_popup').removeClass("opened");
	});
	
	// Share Buttons 
	$('img.ssba').removeAttr("alt");	
	
	// Gallery Navigation	
	$('.gallery_navigation li').click(function() {
	    
	    var selected = $(this).data('gallery');
	    
	    if (!$('.gallery_buckets[data-gallery="' + selected + '"]').hasClass("active")) {
	    
	    	$('.gallery_navigation').find('li.active').removeClass("active");
	    	$('.gallery_buckets.active').removeClass("active");
		    $('.gallery_navigation li[data-gallery="' + selected + '"]').addClass("active");
		    $('.gallery_buckets[data-gallery="' + selected + '"]').addClass("active");
		    
			var sHeight = $('.gallery_buckets_wrapper .gallery_buckets.active').height();
			$('.gallery_buckets_wrapper').css("height", sHeight);
		    
	    } else {
	    
		    return false;
		    
	    }
	     
    });
    
    // Secondary Nav Galleries
    $(".secondary_images").bxSlider({
		slideWidth: 240,
		minSlides: 2,
		maxSlides: 3,
		moveSlides: 1,
		slideMargin: 10,
		pager: false
	});
	
});
