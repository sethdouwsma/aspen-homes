<?php 
/* 
Template Name: About Page
*/
?>

<?php get_header(); ?>
	
	<header>
		<div class="container">
			<h2><?php the_field('hero_header'); ?></h2>
			<?php the_field('hero_paragraph'); ?>
		</div>
		<a href="#" class="scroll_down">scroll down</a>
	</header><!-- end header -->
	
	<div id="main">

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
				<div class="section container">
					<div class="aspen_story_wrapper">
						<h3>The Aspen Story</h3>
						<hr>
						<div class="aspen_story">
							<?php the_field('aspen_story') ?>
						</div>
					</div>
					
					<div class="testimonial_wrapper">
						<img class="testimonial_bg" src="<?php echo get_template_directory_uri(); ?>/images/testimonial_bg.svg" />
						<div class="slider">
							<?php if( have_rows('testimonials') ): ?>
								<ul class="slider__wrapper">
							    <?php while( have_rows('testimonials') ): the_row(); ?>
									<li class="slider__item">
										<p><?php the_sub_field('testimonial'); ?></p>
										<hr>
										<h4><?php the_sub_field('testimonial_name'); ?></h4>
									</li>
							    <?php endwhile; ?>
								</ul>
							<?php endif; ?>
						</div>
					</div>
					
					<div class="our_founders_wrapper">
						<h3>Our Founders</h3>
						<hr>
						<div class="founders_image">
							<img src="<?php the_field('our_founders_image'); ?>" alt="Our Founders">
						</div>
						<div class="our_founders">
							<?php the_field('our_founders') ?>
						</div>
					</div>
				</div>
				
				<div class="section todds_book">
					<div class="container">
						<div class="book_description">
							<h2><?php the_field('header'); ?></h2>
							<?php the_field('description'); ?>
							<?php if(get_field('button_link')):?>
								<a href='<?php the_field('button_link'); ?>' class="button-gold-stroked" target="_blank"><?php the_field('button_text'); ?></a>
							<?php else: ?>
								<a href='<?php echo get_page_link(19); ?>' class="button-gold-stroked"><?php the_field('button_text'); ?></a>
							<?php endif; ?>
						</div>
					</div>
					<div class="book_image">
						<img src="<?php echo get_template_directory_uri(); ?>/images/todds_book.png" alt="Building Your New Dream House">
					</div>
				</div>
				
				<div class="section faq_wrapper container">
					<h3>Frequently Asked Questions</h3>
					<hr>
					<?php if( have_rows('faqs') ): ?>
						<ul class="faq_list">
					    <?php while( have_rows('faqs') ): the_row(); ?>
					        <li class="faq">
					        	<a class="question"><?php the_sub_field('question'); ?></a>
					        	<div class="answer">
					        		<?php the_sub_field('answer'); ?>
					        	</div>
					        </li>
					    <?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>			
				
			</div><!-- end content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
