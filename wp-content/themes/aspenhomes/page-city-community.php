<?php 
/* 
Template Name: City Community Page
*/
?>

<?php get_header(); ?>

	<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$thumb_url = $thumb_url_array[0];
	?>
	
	<header style="background-image: url(<?php echo $thumb_url;?>);">
		<div class="container">
			<h2><?php the_title(); ?></h2>
		</div>
	</header><!-- end header -->
	
	<div id="main">
		<div class="lwood secondary_nav_wrapper not_sticky">
			<?php 
				$args = array(
			 		'post_type'      	=> 'page',
			 		'post_parent'       => $post->ID,                               
				    'order'             => 'ASC',
				    'orderby'           => 'menu_order',
				    'posts_per_page'    => -1
				);
				query_posts($args);
			?>
			<?php if ( have_posts() ) : ?>
				<h4>Communities:</h4>
				<p class="header_toggle_brown"><img src="<?php echo get_template_directory_uri(); ?>/images/header_toggle_brown.svg"></p>
				<ul class="container secondary_nav clearfix">
				<?php while (have_posts()) : the_post(); ?>
					<li><a href="<?php echo '#'.$post->post_name; ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</div>

		<div class="content">
			
			<div class="section comm_intro container">
				<h2>
					<?php 
						if(get_field('tagline')) {
							the_field('tagline');
						} else {
							the_title();
						}	
					?>
				</h2>
				<?php the_field('intro_paragraph'); ?>
			</div>
		 	<?php
			 	$args = array(
			 		'post_type'      	=> 'page',
			 		'post_parent'       => $post->ID,                               
				    'order'             => 'ASC',
				    'orderby'           => 'menu_order',
				    'posts_per_page'    => -1
				);
				query_posts( $args ); 
			?>
		  	<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'community' ); ?>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
			<!-- end single communities -->
			<div id="popup" class="form">
				<div class="overlay_bg"></div>
				<div class="container">
					<a href="#" class="close_popup">&times;</a>
					<?php echo do_shortcode('[contact-form-7 id="242" title="Community Contact Form"]'); ?>
				</div>
			</div>
			<div id="mc_popup">
				<div class="overlay_bg"></div>
				<div class="container">
					<a href="#" class="close_popup">&times;</a>
					<p>Please complete the contact form below to <?php if (is_singular('floor_plan')) { echo "print floor plan."; } else { echo "view information packet."; } ?></p>
					<?php echo do_shortcode('[epm_mailchimp]'); ?>
				</div>
			</div>
		
			<div class="section comm_footer">
				<div class="overlay"></div>
				<div class="comm_footer_bg">
					<?php the_post_thumbnail('original'); ?>
				</div>
				<div class="container">
					<div class="comm_footer_desc">
						<h3>Things To Do In <?php the_title(); ?></h3>
						<hr>
						<?php the_field('things_to_do_description'); ?>
					</div>
					<ul class="comm_footer_list">
						<?php if( have_rows('things_to_do_list') ): ?>
							<?php while( have_rows('things_to_do_list') ): the_row(); ?>
								<li><?php the_sub_field('event'); ?></li>
							<?php endwhile; ?> 
						<?php endif; ?>
					</ul>
				</div>
			</div>
		
		</div><!-- end content -->


<?php get_footer(); ?>
