<?php 
/* 
Template Name: Landing Community Page
*/
?>

<?php get_header(); ?>
	
	<header class="lwood">
		<div class="container">
		<?php if( get_field('hero_header') ): ?>
			<h1><?php the_field('hero_header'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php the_field('hero_paragraph'); ?>
		</div>
	</header><!-- end header -->
	
	<div id="main">
	
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
				<ul class="comm_buckets">
				 	<?php
					 	$args = array(
					 		'post_type'      	=> 'page',
					 		'post_parent'       => $post->ID,                               
						    'order'             => 'ASC',
						    'orderby'           => 'menu_order',
						    'posts_per_page'    => -1
						);
						query_posts( $args ); 
					?>
				  	<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
						<li class="comm_bucket" style="background-image: url('<?php the_field('landing_page_featured_image'); ?>');">
							<div class="overlay"></div>
							<a href="<?php echo the_permalink(); ?>">
								<h3 class="location"><?php the_title(); ?></h3>
								<div class="hover_content">
									<p><img src="<?php echo get_template_directory_uri(); ?>/images/logo_white.svg" alt="Aspen Homes"></p>
									<h3>
										<?php 
											if(get_field('tagline')) {
												the_field('tagline');
											} else {
												the_title();
											}	
										?>								
									</h3>
								</div>
							</a>
						</li>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</ul><!-- end communities -->
			</div><!-- end content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
