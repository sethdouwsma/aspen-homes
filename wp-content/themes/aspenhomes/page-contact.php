<?php 
/* 
Template Name: Contact Page
*/
?>

<?php get_header(); ?>
	
	<header>
		<div id="map"></div>
	</header><!-- end header -->
	
	<div id="main">
	
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="section content container">
				<div class="contact_info_wrapper">
					<h3>Contact Us</h3>
					<hr>
					<div class="contact_info">
						<div class="contact_section">
							<h5>Address:</h5>
							<address>
								<p><?php the_field('address'); ?></p>
								<p><?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip_code'); ?></p>
							</address>
						</div>
						<div class="contact_section">
							<h5>Telephone Numbers:</h5>
							<p>Office: <?php the_field('office_number'); ?></p>
							<p>Fax: <?php the_field('fax_number'); ?></p>
						</div>
						<div class="contact_section">
							<h5>Email:</h5>
							<p><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></p>
						</div>
						<div class="contact_section">
							<?php if( have_rows('social_links',19) ): ?>
								<ul class="social social_gray">
							    <?php while( have_rows('social_links',19) ): the_row(); ?>
									<li><a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php the_sub_field('icon'); ?>"></a></li>
							    <?php endwhile; ?>
							    </ul>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="contact_form_wrapper">	
					<?php echo do_shortcode('[contact-form-7 id="147" title="General Contact Form"]'); ?>
				</div>
			</div><!-- end content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
