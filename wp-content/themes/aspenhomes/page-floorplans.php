<?php 
/* 
Template Name: Floor Plans Page
*/

// Pre-define search params as false
$meta_query = false;
$tax_query = false;
$searching = false;

// Are we searching?
if ( isset( $_GET['aspen-search'] ) )
{

	$searching = true;

	// Prep the custom field (meta) query
	$meta_query = array( 'relation' => 'AND' );

	if ( isset( $_GET['bedrooms'] ) && !empty( $_GET['bedrooms'] ) )
	{
		$meta_query[] = array(
			'key' => 'bedrooms',
			'value' => (int) $_GET['bedrooms'],
			'compare' => '>=',
			'type' => 'NUMERIC'
		);
	}

	if ( isset( $_GET['bathrooms'] ) && !empty( $_GET['bathrooms'] ) )
	{
		$meta_query[] = array(
			'key' => 'bathrooms',
			'value' => (int) $_GET['bathrooms'],
			'compare' => '>=',
			'type' => 'NUMERIC'
		);
	}

	if ( isset( $_GET['sqft'] ) && !empty( $_GET['sqft'] ) )
	{
		// account for the value's representation of the square footage range
		switch ( $_GET['sqft'] )
		{
			case 1:
				$sqft = array( 0, 1999 );
				break;
			case 2:
				$sqft = array( 2000, 2999 );
				break;
			case 3:
				$sqft = 3000;
				break;
		}
		$meta_query[] = array(
			'key' => 'square_feet',
			'value' => $sqft,
			'compare' => is_array( $sqft ) ? 'BETWEEN' : '>=',
			'type' => 'NUMERIC'
		);
	}

	// 
	if ( isset( $_GET['community'] ) && !empty( $_GET['community'] ) )
	{
		$tax_query = array();
		$tax_query[] = array(
			'taxonomy' => 'community',
			'field' => 'slug',
			'terms' => array( $_GET['community'] ),
			'operator' => 'NOT IN'
		);
	}
}
?>

<?php get_header(); ?>
	
	<header class="lwood">
		<div class="container">
		<?php if( get_field('hero_header') ): ?>
			<h1><?php the_field('hero_header'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php the_field('hero_paragraph'); ?>
		</div>
	</header><!-- end header -->
	
	<div id="main">

	<?php if ( have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content section container">
				<?php get_sidebar('floor_plan'); ?>
				
				<?php $args = array(
			            'posts_per_page' => 9999,
			            'post_type'  =>  'floor_plan',
			            'meta_query' => array(
					        array(
					            'key' => 'feature_this_floor_plan',
					            'value' => '1',
					            'compare' => 'LIKE'
					        )
					    ),
						'meta_key'   =>  'square_feet', // we're sorting by the square footage
						'orderby'    =>  'meta_value_num', // it's a numeric value
						'order'      =>  'DESC' // largest to smallest
			        );

					$featured = new WP_Query( $args );
			    ?>
			    
			    <?php $args2 = array(
			            'posts_per_page' => 9999,
			            'post_type'  =>  'floor_plan',
			            'meta_query' => array(
					        array(
					            'key' => 'feature_this_floor_plan',
					            'value' => '0',
					            'compare' => 'LIKE'
					        )
					    ),
						'meta_key'   =>  'square_feet', // we're sorting by the square footage
						'orderby'    =>  'meta_value_num', // it's a numeric value
						'order'      =>  'DESC' // largest to smallest
			        );
			        

					// Added search params
					if ( $meta_query ) {

						$meta_query[] = array(
							'key' => 'feature_this_floor_plan',
							'value' => '1',
							'compare' => 'LIKE'
						);
						$args['meta_query'] = $meta_query;

						array_pop( $meta_query );

						$meta_query[] = array(
							'key' => 'feature_this_floor_plan',
							'value' => '0',
							'compare' => 'LIKE'
						);
						$args2['meta_query'] = $meta_query;
					}

					if ( $tax_query )
					{
						$args['tax_query'] = $tax_query;
						$args2['tax_query'] = $tax_query;
					}

					$featured = new WP_Query( $args );
					$floorplans = new WP_Query( $args2 );
			    ?>

				<?php if ( $searching ) : ?>
				
					<?php if ( $featured->have_posts() || $floorplans->have_posts() ) : ?>
					
						<h3 class="search_header">Search Results: (<?php echo intval( $featured->found_posts + $floorplans->found_posts ); ?> Floor Plans)</h3>

					<?php else : ?>

						<h3 class="search_header">No Floor Plans Found</h3>

					<?php endif; ?>
					
					<ul class="floorplan_buckets">
		
				        <?php while ( $featured->have_posts() ) : $featured->the_post(); ?>
	
							<?php get_template_part( 'content', 'floorplan' ); ?>
						
						<?php endwhile; ?>
	
				        <?php while ( $floorplans->have_posts() ) : $floorplans->the_post(); ?>
	
							<?php get_template_part( 'content', 'floorplan' ); ?>
						
						<?php endwhile; ?>
			
					</ul><!-- end floorplans -->
					
					<?php else: ?>
					
						<ul class="floorplan_buckets">
		
					        <?php while ( $featured->have_posts() ) : $featured->the_post(); ?>
		
								<?php get_template_part( 'content', 'floorplan' ); ?>
							
							<?php endwhile; ?>
		
					        <?php while ( $floorplans->have_posts() ) : $floorplans->the_post(); ?>
		
								<?php get_template_part( 'content', 'floorplan' ); ?>
							
							<?php endwhile; ?>
		
						</ul><!-- end floorplans -->

				<?php endif; ?>
				
				<?php wp_reset_query(); ?>

			</div><!-- end content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
