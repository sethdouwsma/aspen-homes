<?php 
/* 
Template Name: Galleries Page
*/
?>

<?php get_header(); ?>
	
	<header class="lwood">
		<div class="container">
		<?php if( get_field('hero_header') ): ?>
			<h1><?php the_field('hero_header'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php the_field('hero_paragraph'); ?>
		</div>
	</header><!-- end header -->
	
	<div id="main">

		<div class="section content container aligncenter">
			
			<?php 
				$terms = get_terms('gallery'); 
				$count = 1;
				$flag = 1;
			?>
			<?php if ( !empty( $terms ) && !is_wp_error( $terms ) ): ?>
				<ul class="gallery_navigation clearfix">
					<?php foreach ( $terms as $term ) : ?>
						<?php if ($flag == 1): ?>
							<li class="gallery_nav_item active" data-gallery="<?php echo $count++ ?>"><?php echo $term->name ?></li>
							<?php $flag = 0; ?>
						<?php else : ?>
							<li class="gallery_nav_item" data-gallery="<?php echo $count++ ?>"><?php echo $term->name ?></li>
						<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
			
			
			<div class="gallery_buckets_wrapper">
			
			<?php $taxonomies = get_object_taxonomies( (object) array( 'post_type' => 'gallery' ) ); ?>
			<?php foreach( $taxonomies as $taxonomy ) : ?>
			
			    <?php 
			    	$terms = get_terms( $taxonomy ); 
					$count = 1;
					$flag = 1;
				?>
			    <?php foreach( $terms as $term ) : ?>
				 	<?php
					 	$args = array(   
					 		'post_type'      	=> 'gallery',                    
						    'order'             => 'ASC',
						    'orderby'           => 'menu_order',
						    'posts_per_page'    => -1,
				            'tax_query' => array(
				                'relation' => 'AND',
				                array(
				                    'taxonomy' => $taxonomy,
				                    'field' => 'slug',
				                    'terms' => $term->slug
				                )
				            ),
						);
						$posts = new WP_Query( $args ); 
					?>
			
			        <?php if( $posts->have_posts() ): ?>
						<?php if ($flag == 1): ?>
							<ul class="gallery_buckets active" data-gallery="<?php echo $count++ ?>">
							<?php $flag = 0; ?>
						<?php else: ?>
							<ul class="gallery_buckets" data-gallery="<?php echo $count++ ?>">
						<?php endif; ?>
					        	<?php while( $posts->have_posts() ) : $posts->the_post(); ?>
							        <?php 
										$thumb_id = get_post_thumbnail_id();
										$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'original', true);
										$thumb_url = $thumb_url_array[0];
									?>
									<li class="gallery_bucket" style="background-image: url(<?php echo $thumb_url?>);">
										<a href="<?php echo the_permalink(); ?>">
											<h4 class="gallery_title"><?php the_title(); ?></h4>
										</a>
									</li>
								<?php endwhile; ?>
							</ul>
			        <?php endif; ?>
			
			    <?php endforeach; ?>
			
			<?php endforeach; ?>
				
			</div><!-- end floorplans -->
			
		</div><!-- end content -->

<?php get_footer(); ?>
