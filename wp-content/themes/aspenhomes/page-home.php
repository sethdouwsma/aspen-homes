<?php 
/* 
Template Name: Home Page
*/
?>

<?php get_header(); ?>
	
	<header>
		<div class="overlay"></div>
		<div class="video_container" style="background-image: url(<?php the_field('video_background'); ?>);">
			<video autoplay loop poster="<?php echo get_template_directory_uri(); ?>/images/video_placeholder_2.jpg" id="video_bg">
				<source src="<?php echo get_template_directory_uri(); ?>/videos/aspen_homes.mp4" type="video/mp4">
				<source src="<?php echo get_template_directory_uri(); ?>/videos/aspen_homes.webm" type="video/webm">
				<source src="<?php echo get_template_directory_uri(); ?>/videos/aspen_homes.ogv" type="video/ogv">
			</video>
			<!--[if lt IE 9]>
			<script>
				document.createElement('video');
			</script>
			<![endif]-->
		</div>
		<div class="container">
			<h1><?php the_field('tagline'); ?></h1>
		</div>
		<a href="#" class="scroll_down">scroll down</a>
	</header><!-- end header -->
	
	<div id="main">

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
				<div class="section lwood">
					<div class="container">
						<h3><?php the_field('buckets_header'); ?></h3>
						<hr>
						<ul class="hp_buckets">
							<li>
								<div class="bucket_bg floorplans_bucket">
									<a href="<?php echo get_permalink('7') ?>">Learn More</a>
									<div class="overlay"></div>
									<div class="bucket_content">
										<p>Floor Plans</p>
										<hr>
									</div>
								</div>
								<div class="description">
									<p><?php the_field('floor_plans_description'); ?></p>
									<a href="<?php echo get_permalink('7') ?>">Learn More</a> 
								</div>
							</li>	
							<li>
								<div class="bucket_bg communities_bucket">
									<a href="<?php echo get_permalink('9') ?>">Learn More</a>
									<div class="overlay"></div>
									<div class="bucket_content">
										<p>Communities</p>
										<hr>
									</div>
								</div>
								<div class="description">
									<p><?php the_field('communities_description'); ?></p>
									<a href="<?php echo get_permalink('9') ?>">Learn More</a> 
								</div>
							</li>	
							<li>
								<div class="bucket_bg movein_bucket">
									<a href="<?php echo get_permalink('11') ?>">Learn More</a> 
									<div class="overlay"></div>
									<div class="bucket_content">
										<p>Move-In Ready</p>
										<hr>
									</div>
								</div>
								<div class="description">
									<p><?php the_field('move_in_ready_description'); ?></p>
									<a href="<?php echo get_permalink('11') ?>">Learn More</a> 
								</div>
							</li>						
						</ul>
					</div>
				</div>
				
				<div class="section container">
					<div class="why_us">
						<h3><?php the_field('why_header'); ?></h3>
						<p><?php the_field('why_block'); ?></p>
					</div>
					<div class="values">
						<h3><?php the_field('values_header'); ?></h3>
						
						<?php if( have_rows('values') ): ?>
							<ul>
								<?php while( have_rows('values') ): the_row(); ?>
									<li><?php the_sub_field('value'); ?></li>
								<?php endwhile; ?> 
							</ul>
						<?php endif; ?>
					</div>
				</div>
				
				<div class="section aspen_difference">
					<div class="container">
						<h2><?php the_field('aspen_difference_header'); ?></h2>
						<?php the_field('aspen_difference_body_content'); ?>
						<a href="<?php the_field('aspen_difference_link') ?>" class="button-gold-stroked">Read More</a>
					</div>
				</div>
				<div class="gray featured_in">
					<ul class="container">
						<li>As Featured Throughout:</li>
						<li class="hgtv"><a href="http://youtu.be/09kyn6qBOeA" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/hgtv.png" alt="HGTV - Aspen Homes"></a></li>
						<li class="mt"><a href="http://eeditions.shoom.com/doc/coeur-d-alene/mining/2014033102/#0" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/mining_and_timber.png" alt="Mining & Timber - Aspen Homes"></a></li>
						<li class="cda"><a href="http://issuu.com/aspenhomes/docs/cda_magazine_2004_article?e=12638439/8849312" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/cda.png" alt="Coeur d'Alene Magazine - Aspen Homes"></a></li>
						<li class="builder"><img src="<?php echo get_template_directory_uri(); ?>/images/builder.png" alt="Builder - Aspen Homes"></li>
					</ul>
				</div>
				
				<div class="section hp_panels container clearfix">
					<div class="row">
						<div class="row_image">
							<a href="#" id="video_button" class="popup_button"><img src="<?php echo get_template_directory_uri(); ?>/images/pj_video.jpg" alt="Project Viewer"></a>
							<div id="popup">
								<div class="overlay_bg"></div>
								<div class="container">
									<a href="#" class="close_popup">&times;</a>
									<div class="pj_video">
										<iframe id="pj_video" width="750" height="422" src="" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>
							</div>
						</div>
						<div class="row_description">
							<h3>Project Viewer</h3>
							<hr>
							<p>Aspen Homes has acquired “Project View”, a web-based project management tool. This empowers Aspen Homes to provide a better building experience for their homebuyers, associates, and subcontractors. The system provides real-time 24/7 access to scheduling information, change orders, documents, photos, and much more. Click on the video to see a Demonstration.</p>
							<a href="#" class="pj_button button-gold-stroked" tagret="_blank">Go To Project Viewer</a>
						</div>
					</div>
					<div class="row right best_of">
						<div class="row_image">
							<img src="<?php echo get_template_directory_uri(); ?>/images/hp_best_of.png" alt="Voted Best of 2014">
						</div>
						<div class="row_description">
							<h3>Voted Best of 2014</h3>
							<hr>
							<p>We’re proud to have been awarded the Business Journal’s 2014 Best Residential Builder People’s Choice Award. This award distinguishes the “Aspen Difference” for superior quality, service and process. We’ve always held ourselves at a high standard, and its great to know that it shows. At Aspen Homes, we’re committed to open, honest communication that makes the customer experience exciting and rewarding.</p>
							<a href="<?php echo get_post_permalink(364); ?>" class="button-gold-stroked">Read More</a>
						</div>
					</div>
					<div class="row best_of">
						<div class="row_image">
							<img src="<?php echo get_template_directory_uri(); ?>/images/registered_master_builder.jpg" alt="Registered Master Builder">
						</div>
						<div class="row_description">
							<h3>Registered Master Builder</h3>
							<hr>
							<p>Aspen Homes is honored to have received the distinguished recognition as one of Idaho’s Registered Master Builders in 2014. This mark of excellence is distributed by the Idaho Building Contractors Association, and Aspen Homes is the only residential builder in North Idaho to receive this status. </p>
							<a href="<?php echo get_post_permalink(362); ?>" class="button-gold-stroked">Read More</a>
						</div>
					</div>
				</div>
				
				<div class="section hp_news">
					<div class="container">
						<h3>Aspen Homes News</h3>
						<hr>
						<div class="hp_news_slider slider">
						  <ul class="slider__wrapper">
						  	<?php
							 	$args = array(
						            'posts_per_page' => 3,
						            'meta_query' => array(
								        array(
								            'key' => 'feature_this_article',
								            'value' => '1',
								            'compare' => 'LIKE'
								        )
								    ),
								    'orderby'	=> 'date',
									'order'		=> 'DESC' //  Newst To Oldest
						        );
								query_posts( $args ); 
							?>
						  	<?php if ( have_posts() ) : ?>
								<?php while ( have_posts() ) : the_post(); ?>
								<li class="slider__item">
									<?php get_template_part( 'content', get_post_format() ); ?>
								</li>
							<?php endwhile; ?>
						  	<?php endif; ?>
							<?php wp_reset_query(); ?>
						  </ul>
						</div>
					</div>
				</div>
				
				<div class="section community_links container">
					<h3>Get to know where we live</h3>
					<hr>
					<?php if(have_rows('link_section')): ?>
						<?php while( have_rows('link_section') ): the_row(); ?>
							<div class="cl_section">
								<ul>
									<li><h5><?php the_sub_field('link_section_title'); ?></h5></li>
								<?php while( have_rows('links') ): the_row(); ?>
									<li><a href="<?php the_sub_field('url'); ?>" target="_blank"><?php the_sub_field('link_title'); ?></a></li>
								<?php endwhile; ?>
								</ul>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				
			</div><!-- end content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
