<?php 
/* 
Template Name: Move-In Ready Page
*/
?>

<?php get_header(); ?>
	
	<header class="lwood">
		<div class="container">
		<?php if( get_field('hero_header') ): ?>
			<h1><?php the_field('hero_header'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php the_field('hero_paragraph'); ?>
		</div>
	</header><!-- end header -->
	
	<div id="main">

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
				<ul class="mi_buckets">
				 	<?php
					 	$args = array(
				            'posts_per_page' => -1,
				            'post_type' => 'move_in_ready',
				            'order'	=> 'DESC',
				            'orderby' => 'meta_value',
							'meta_key' => 'price',
						);
						query_posts( $args ); 
					?>
				  	<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>

						<?php 
							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
						?>
						
						<li class="mi_bucket">
							<?php if(get_field('pending')): ?>
								<div class="mi_bucket_image pending" style="background-image: url(<?php echo $thumb_url;?>);">
									<a href="<?php echo the_permalink(); ?>">
										<div class="overlay"></div>
										<span><?php the_field('floor_plan_model'); ?></span>
										<img src="<?php echo get_template_directory_uri(); ?>/images/pending.svg" class="pending">
									</a>
								</div>
							<?php elseif(get_field('sold')): ?>
								<div class="mi_bucket_image sold" style="background-image: url(<?php echo $thumb_url;?>);">
									<a href="<?php echo the_permalink(); ?>">
										<div class="overlay"></div>
										<span><?php the_field('floor_plan_model'); ?></span>
										<img src="<?php echo get_template_directory_uri(); ?>/images/sold.svg" class="sold">
									</a>
								</div>
							<?php else: ?>
								<div class="mi_bucket_image" style="background-image: url(<?php echo $thumb_url;?>);">
									<a href="<?php echo the_permalink(); ?>">
										<div class="overlay"></div>
										<span><?php the_field('floor_plan_model'); ?></span>
									</a>
								</div>
							<?php endif; ?>
							<div class="mi_bucket_info_wrapper dwood">
								<div class="mi_bucket_header">
									<h3><?php the_field('floor_plan_model'); ?></h3>
									<?php if(get_field('under_construction')): ?>
										<p class="under_construction">Under Construction</p>
									<?php endif; ?>
									<address>
										<p><?php the_field('address'); ?></p>
										<p><?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip_code'); ?></p>
									</address>
								</div>
								<ul class="mi_bucket_info">
									<li class="mi_bucket_sqft sqft"><?php $field = get_field('square_footage'); echo number_format($field); ?> sq. ft.</em></p>
									<li><?php the_field('bedrooms'); ?> Bedrooms</li>
									<li><?php the_field('bathrooms'); ?> Bathrooms</li>
									<li><?php the_field('car_garage'); ?> Car Garage</em></li>
									<?php if( have_rows('extras') ): ?>
									    <?php while( have_rows('extras') ): the_row(); ?>
									        <li><?php the_sub_field('extra'); ?></li>
									    <?php endwhile; ?>
									<?php endif; ?>
									<li class="mi_bucket_price"><h3 class="price"><?php if(get_field('price')) { $field = get_field('price'); echo '$'.number_format($field); } ?></h3></li>
								</ul>
								<?php if(!get_field('sold')): ?>
									<p class="mi_bucket_button"><a href="<?php echo get_permalink(); ?>" class="button-gold-stroked">View Details</a></p>
								<?php endif; ?>
							</div>
						</li>
						<?php endwhile; ?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
		
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</ul><!-- end floorplans -->
			</div><!-- end content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
