<?php 
/* 
Template Name: Single Community Page
*/
?>

<?php get_header(); ?>

	<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$thumb_url = $thumb_url_array[0];
	?>
	
	<header style="background-image: url(<?php echo $thumb_url;?>);">
		<div class="container">
			<h2><?php the_title(); ?></h2>
		</div>
	</header><!-- end header -->
	
	<div id="main">
		<div class="lwood secondary_nav_wrapper">
			<?php 
				global $post; $thispage = $post->ID;
				$pagekids = get_pages("child_of=".$thispage."&sort_column=menu_order");
			?>
			<?php if ($pagekids) : ?>
				<ul class="container secondary_nav clearfix">
					<?php wp_list_pages("depth=1&title_li=&sort_column=menu_order&child_of=".$thispage); ?>
				</ul>
			<?php endif; ?>
		</div>

		<div class="content">
			
			<div class="section comm_intro container">
				<h2><?php the_field('intro_header'); ?></h2>
				<?php the_field('intro_paragraph'); ?>
			</div>
		
			<div class="section comm_footer">
				<div class="comm_footer_bg">
					<?php the_post_thumbnail('original'); ?>
				</div>
				<div class="container">
					<div class="comm_footer_desc">
						<h3>Things To Do In <?php the_title(); ?></h3>
						<hr>
						<?php the_field('things_to_do_description'); ?>
					</div>
					<ul class="comm_footer_list">
						<?php if( have_rows('things_to_do_list') ): ?>
							<?php while( have_rows('things_to_do_list') ): the_row(); ?>
								<li><?php the_sub_field('event'); ?></li>
							<?php endwhile; ?> 
						<?php endif; ?>
					</ul>
				</div>
			</div>
		
		</div><!-- end content -->


<?php get_footer(); ?>
