<?php get_header(); ?>
	
	<header class="lwood">
		<div class="container">
		<?php if( get_field('hero_header') ): ?>
			<h1><?php the_field('hero_header'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php the_field('hero_paragraph'); ?>
		</div>
	</header><!-- end header -->

	<div id="main">
	
		<div class="section content container">	
					
			<?php if ( have_posts() ) : ?>
			
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
	
			<?php endif; ?>
			
		</div><!-- end .content -->

<?php get_footer(); ?>