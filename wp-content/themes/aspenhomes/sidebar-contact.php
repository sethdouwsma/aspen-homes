<?php
// Main Sidebar
?>
	<div class="sidebar">
		<div class="sidebar_questions alignleft">
			<h4>Have More Questions?</h4>
			<p>Get in touch with us, we'd love to hear from you.</p>
			<div class="sidebar_address">
				<h5>Integrated Barcoding</h5>
				<p>PO Box 156</p>
				<p>Harbor Springs, MI 49740</p>
				<p><a href="mailto:info@intbarcoding.com">info@intbarcoding.com</a></p>
				<p><span>616-988-7788</span></p>
			</div>
		</div>
	</div><!-- .sidebar -->
