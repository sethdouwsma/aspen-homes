<?php
// Main Sidebar

$bedrooms = false;
if ( isset( $_GET['bedrooms'] ) )
{
	$bedrooms = $_GET['bedrooms'];
}

$bathrooms = false;
if ( isset( $_GET['bathrooms'] ) )
{
	$bathrooms = $_GET['bathrooms'];
}

$sqft = false;
if ( isset( $_GET['sqft'] ) )
{
	$sqft = $_GET['sqft'];
}

$community = false;
if ( isset( $_GET['community'] ) )
{
	$community = $_GET['community'];
}

?>

	<?php $notify = "";
	if(isset($_GET['notify_box'])){ $notify = $_GET['notify_box']; } ?>
	<div class="sidebar fp_filters">
		<div class="seachform-floorplan search clearfix" rol="search">
			<form role="search" action="<?php echo site_url('/'); ?>" method="get">
				<input type="search" class="search_input" name="s" placeholder="Search Floor Plans..."/>
				<input type="hidden" name="post_type" value="floor_plan" /> <!-- // hidden 'your_custom_post_type' value -->
				<input type="submit" class="button-gold-solid" alt="Search" value="Search" />
			</form>
		</div><!-- .search -->
		
		<div class="filter-floorplan filter" rol="filter">
			<form role="filter" action="<?php echo site_url( '/floor-plans/' ); ?>" method="get">
		        <div class="field_group">
		            <select name="bedrooms">
		                <option value="">Select Bedrooms</option>
		                <?php for ( $i = 1; $i <= 5; $i++ ) : ?>
		                <option value="<?php echo (int) $i; ?>"<?php selected( $i, $bedrooms ); ?>><?php echo (int) $i; ?>+ Bedrooms</option>
		                <?php endfor; ?>
		            </select>
		        </div>
		        <div class="field_group">
		            <select name="bathrooms">
		                <option value="">Select Bathrooms</option>
		                <?php for ( $i = 1; $i <= 5; $i++ ) : ?>
		                <option value="<?php echo (int) $i; ?>"<?php selected( $i, $bathrooms ); ?>><?php echo (int) $i; ?>+ Bathrooms</option>
		                <?php endfor; ?>

		            </select>
		        </div>
		        <div class="field_group">
		            <select name="sqft">
		                <option value="" selected="selected">Select Sq. Footage</option>
		                <option value="1"<?php selected( 1, $sqft ); ?>>0 - 1999 Sq. Ft.</option>
		                <option value="2"<?php selected( 2, $sqft ); ?>>2000 - 2999 Sq. Ft.</option>
		                <option value="3"<?php selected( 3, $sqft ); ?>>3000+ Sq. Ft.</option>
		            </select>
		        </div>
		        <div class="field_group">
		            <select name="community">
		                <option value="" selected="selected">Select Community</option>
		                <?php 
		                	global $post; 
		                	$terms = get_terms( 'community', 'orderby=count&hide_empty=0&exclude=56,57,63,58' ); ?>
		                <?php foreach ($terms as $term) : setup_postdata($post); ?>
		                	<option value="<?php echo esc_attr( $term->slug ); ?>"<?php selected( $term->slug, $community ); ?>><?php echo $term->name ?></option>
		                <?php endforeach; ?>
		            </select>
		        </div>
		        <div class="field_submit">
		            <input type="submit" class="button-gold-solid" alt="Find My Home" value="Find My Home">
		            <a href="<?php echo get_page_link(7); ?>" class="button-brown-solid">Clear Search Results</a>
		            <input type="hidden" name="aspen-search" value="1">
		        </div>
		    </form>
		</div><!-- .filter -->
	</div><!-- .sidebar -->
