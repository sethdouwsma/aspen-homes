<?php
// Main Sidebar
?>
	<div class="sidebar">
		<div class="sidebar_section">
			<h4>Categories</h4>
			<ul class="categories">
				<?php wp_list_categories('title_li='); ?> 
			</ul>
		</div>
		
		<div class="sidebar_section">
			<h4>Archives</h4>
			<ul class="archives">
				<?php wp_get_archives(); ?> 
			</ul>
		</div>
	</div><!-- .sidebar -->
