<?php 
/* 
Displays Case Studies Single Content
*/
?>

<?php get_header(); ?>

		<div class="section content">
			<h2 class="aligncenter"><?php the_title(); ?></h2>
			<article class="cs_body container">
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>		
			</article>
			
			<?php if( get_field('testimonial') ): ?>
				<div class="testimonial section gray aligncenter">
					<div class="container">
						<h2>Some Positive Feedback</h2>
						<p><?php the_field('testimonial'); ?></p>
						<p>— <?php the_field('testimonial_name'); ?></p>
					</div>
				</div>
			<?php endif; ?>
			
			<div class="container aligncenter">
				<h2>Other Case Studies</h2>
				<ul class="case_study_list">
					<?php 
					    query_posts(array( 
					        'post_type' => 'case_study',
							'orderby' => 'rand',
					        'showposts' => 3 
					    ) );
					?>
					<?php while (have_posts()) : the_post(); ?>
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
						<li style="background-image: url(<?php echo $feat_image; ?>);">
							<a href="<?php the_permalink() ?>"></a>
							<div class="overlay"></div>
							<div class="container">
						        <h4><?php the_title(); ?></h4>
						        <a href="<?php the_permalink() ?>" class="button green">Read More</a>
							</div>
						</li>
					<?php endwhile;?>
				</ul>
			</div>
		</div><!-- end .content -->
				

<?php get_footer(); ?>