<?php 
/* 
Displays Single Floor Plan Page
*/
?>

<?php get_header(); ?>
	
	<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'original', true);
		$thumb_url = $thumb_url_array[0];
	?>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
	
	<header style="background-image: url(<?php echo $thumb_url;?>);">
		<div class="overlay"></div>
		<div class="container">
			<h2><?php the_title(); ?></h2>
		</div>
		
		<div class="fp_post_nav cleafix">
			<div class="fp_post_nav_item previous">
				<?php next_post_link('%link', 'Prev Home') ?>
			</div>
			<div class="fp_post_nav_item next">
				<?php previous_post_link('%link', 'Next Home') ?>
			</div>
		</div> <!-- end navigation -->
	</header><!-- end header -->
	
	<div id="main">
			<div class="lwood details_wrapper">
				<ul class="container details clearfix">
					<li><?php the_field('bedrooms'); ?> Bedrooms</li>
					<li><?php the_field('bathrooms'); ?> Bathrooms</li>
					<li class="sqft"><em><?php $field = get_field('square_feet'); echo number_format($field); ?> sq. ft.</em></li>
					<li><?php the_field('car_garage'); ?> Car Garage</em></li>
					<?php if( have_rows('extras') ): ?>
					    <?php while( have_rows('extras') ): the_row(); ?>
					        <li><?php the_sub_field('extra'); ?></li>
					    <?php endwhile; ?>
					<?php endif; ?>
				</ul>
			</div>

			<div class="content">
				<div class="section floorplan_images_wrapper container">
					<h3>Floor Plans</h3>
					<hr>
					<?php if( have_rows('floor_plan_images') ): ?>
						<ul class="floorplan_images clearfix">
						    <?php while( have_rows('floor_plan_images') ): the_row(); ?>
						    	<?php $image = get_sub_field('image'); ?>
								<?php if($image): //dont output an empty image tag ?>
								<li><a href="<?php echo $image['url']; ?>" data-lightbox="floorplans" data-title="<?php echo $image['caption']; ?>"><img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['caption']; ?>" /></a></li>
								<?php endif; ?>
						    <?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
				
				<?php if( have_rows('model_features') || have_rows('secondary_images')): ?>
				<div class="small_section features_wrapper dwood">
					<div class="container">
						<ul class="features clearfix">
							<h3>Standard Features</h3>
							<hr>
						    <?php while( have_rows('model_features') ): the_row(); ?>
						        <li><?php the_sub_field('feature'); ?></li>
						    <?php endwhile; ?>
						</ul>
						<ul class="secondary_images clearfix">
						    <?php while( have_rows('secondary_images') ): the_row(); ?>
						    	<?php $image = get_sub_field('image'); ?>
								<?php if($image): //dont output an empty image tag ?>
								<li><a href="<?php echo $image['url']; ?>" data-lightbox="secondary-images" data-title="<?php echo $image['caption']; ?>"><img src="<?php echo $image['sizes']['secondary-thumb']; ?>" alt="<?php echo $image['caption']; ?>" /></a></li>
								<?php endif; ?>
						    <?php endwhile; ?>
						</ul>
					<?php endif; ?>
					</div>
				</div>
				
				<div class="section container aligncenter">
					<div class="floorplan_featured_image">
						<a href="<?php echo $thumb_url; ?>" data-lightbox="<?php the_title(); ?>">
							<?php the_post_thumbnail('floorplan-large'); ?>
						</a>
					</div>
				</div>
				
				<div class="small_section lwood">
					<div class="container floorplan_footer">
						<div class="floorplan_buttons">
							<h3>Want More Information?</h3>
							<hr>
							<a href="<?php the_title(); ?>" class="popup_button button-gold-stroked">Inquire</a>
							<a href="#" id="print" class="mc_button button-gold-stroked">Print Floor Plan</a>
						</div>
					</div>
				</div>
			</div><!-- end content -->
			<div id="popup" class="form">
				<div class="overlay_bg"></div>
				<div class="container">
					<a href="#" class="close_popup">&times;</a>
				</div>
			</div>
			<div id="mc_popup">
				<div class="overlay_bg"></div>
				<div class="container">
					<a href="#" class="close_popup">&times;</a>
					<p>Please complete the contact form below to <?php if (is_singular('floor_plan')) { echo "print floor plan."; } else { echo "view information packet."; } ?></p>
					<?php echo do_shortcode('[epm_mailchimp]'); ?>
				</div>
			</div>
			
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
