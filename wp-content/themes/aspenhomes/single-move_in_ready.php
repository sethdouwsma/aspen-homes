<?php 
/* 
Displays Single Move In Ready Home
*/
?>

<?php get_header(); ?>
	
	<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'original', true);
		$thumb_url = $thumb_url_array[0];
	?>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
	
	<header style="background-image: url(<?php echo $thumb_url;?>);">
		<div class="overlay"></div>
		<div class="container">
			<?php if(get_field('pending')): ?>
				<img src="<?php echo get_template_directory_uri(); ?>/images/pending.svg" class="pending">
			<?php elseif(get_field('sold')): ?>
				<img src="<?php echo get_template_directory_uri(); ?>/images/sold.svg" class="sold">
			<?php endif; ?>
			<h2><?php the_field('floor_plan_model'); ?></h2>
			<?php if(get_field('under_construction')): ?>
				<p class="under_construction">Under Construction</p>
			<?php endif; ?>
		</div>
		
		<div class="fp_post_nav cleafix">
			<div class="fp_post_nav_item previous">
				<?php next_post_link('%link', 'Prev Home') ?>
			</div>
			<div class="fp_post_nav_item next">
				<?php previous_post_link('%link', 'Next Home') ?>
			</div>
		</div> <!-- end navigation -->
	</header><!-- end header -->
	
	<div id="main">
			<div class="lwood details_wrapper">
				<ul class="container details clearfix">
					<li><?php the_field('bedrooms'); ?> Bedrooms</li>
					<li><?php the_field('bathrooms'); ?> Bathrooms</li>
					<?php if(get_field('price')) { $field = get_field('price'); echo '<li class="price">$'.number_format($field).'</li>'; } ?>
					<li><?php the_field('car_garage'); ?> Car Garage</li>
					<li class="sqft"><?php $field = get_field('square_footage'); echo number_format($field); ?> sq. ft.</li>
					<?php if( have_rows('extras') ): ?>
					    <?php while( have_rows('extras') ): the_row(); ?>
					        <li><?php the_sub_field('extra'); ?></li>
					    <?php endwhile; ?>
					<?php endif; ?>
				</ul>
			</div>

			<div class="content">
				<div class="section floorplan_images_wrapper container">
					<div class="mi_image">
						<?php if( have_rows('secondary_images') ): ?>
						    <?php while( have_rows('secondary_images') ): the_row(); ?>
						    	<?php $image = get_sub_field('image'); ?>
								<?php if($image): //dont output an empty image tag ?>
								<a href="<?php echo $image['url']; ?>" data-lightbox="secondary-images" data-title="<?php echo $image['caption']; ?>"><img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['caption']; ?>" /></a>
								<?php endif; ?>
						    <?php break; endwhile; ?>
						<?php endif; ?>
					</div>
					<div class="mi_description">
						<h3>
							<address>
								<p><?php the_field('address'); ?></p>
								<p><?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip_code'); ?></p>
							</address>
						</h3>
						<p class="mi_meta">
							<?php $terms = get_the_terms( $post->ID, 'community' ); ?>
							<?php if ( !empty( $terms ) && !is_wp_error( $terms ) ): ?>
								<?php foreach( $terms as $term ) : ?>
								<?php 
									$termid = $term->parent;
									$parent = get_term_by("id", $termid, "community");
									
								?>
								<span class="community">Community: <a href="/communities/<?php echo $parent->slug; ?>/#<?php echo $term->slug ?>"><?php echo $term->name ?></a></span>
								<?php endforeach; ?>
							<?php endif; ?> <span class="map_it"><a href="http://maps.google.com/?q=<?php the_title(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/map_it_icon.png"> Map It</a></span></p>
						<hr>
						<?php the_field('description'); ?>
					</div>
				</div>
				
				<div class="small_section features_wrapper dwood">
					<div class="container">
						<?php if( have_rows('model_features') ): ?>
							<ul class="features clearfix">
								<h3>Home Includes</h3>
								<hr>
							    <?php while( have_rows('model_features') ): the_row(); ?>
							        <li><?php the_sub_field('feature'); ?></li>
							    <?php endwhile; ?>
							</ul>
						<?php endif; ?>
						<?php if( have_rows('secondary_images') ): ?>
						<ul class="secondary_images clearfix">
						    <?php while( have_rows('secondary_images') ): the_row(); ?>
						    	<?php $image = get_sub_field('image'); ?>
								<?php if($image): //dont output an empty image tag ?>
								<li><a href="<?php echo $image['url']; ?>" data-lightbox="secondary-images" data-title="<?php echo $image['caption']; ?>"><img src="<?php echo $image['sizes']['secondary-thumb']; ?>" alt="<?php echo $image['caption']; ?>" /></a></li>
								<?php endif; ?>
						    <?php endwhile; ?>
						</ul>
					<?php endif; ?>
					</div>
				</div>
				
				<div class="section floorplan_images_wrapper container">
					<h3>Floor Plans</h3>
					<hr>
					<?php if( have_rows('floor_plan_images') ): ?>
						<ul class="floorplan_images clearfix">
						    <?php while( have_rows('floor_plan_images') ): the_row(); ?>
						    	<?php $image = get_sub_field('image');?>
								<?php if($image): //dont output an empty image tag ?>
								<li><a href="<?php echo $image['url']; ?>" data-lightbox="floorplans" data-title="<?php echo $image['caption']; ?>"><img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['caption']; ?>" /></a></li>
								<?php endif; ?>
						    <?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
				
				<div class="small_section lwood">
					<div class="container mi_footer">
						<div class="listing_info">
							<h3>Listing Information</h3>
							<hr>
							<ul>
								<li><span>Broker:</span> <?php the_field('broker'); ?></li>
								<li><span>Agent:</span> <?php the_field('agent'); ?></li>
								<li><span>Phone:</span> <?php the_field('agent_phone'); ?></li>
							</ul>
					    </div>
						
						<div class="mi_buttons">
							<a href="<?php the_field('listing_url'); ?>" target="_blank" class="button-gold-stroked">View Listing</a>
						</div>
					</div>
				</div>
			</div><!-- end content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
