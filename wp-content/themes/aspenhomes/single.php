<?php get_header(); ?>

	<div id="main">
	
		<div class="section content container">	
					
			<?php if ( have_posts() ) : ?>
			
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>
	
			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
	
			<?php endif; ?>
			
		</div><!-- end .content -->

<?php get_footer(); ?>